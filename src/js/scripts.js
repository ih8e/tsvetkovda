window.onload = () => {
    // Navbar scroll
    document.addEventListener("click", e => {
        if (e.target.className.match(/navbar__item/)) {
            let targetPage = "." + e.target.className.match(/item_(\w+)/)[1];
            document.querySelector(targetPage).scrollIntoView({ behavior: "smooth" });
        }
    });

    // Button scroll
    document.querySelector(".intro__expand-button").addEventListener("click", () => {
        document.querySelector(".about").scrollIntoView({ behavior: "smooth" });
    });

    // Send to Telegram
    document.querySelector(".feedback__form").addEventListener("submit", function(e) {
        e.preventDefault();

        let name = document.querySelector(".form__input-name").value;
        let email = document.querySelector(".form__input-email").value;
        let msg = document.querySelector(".form__message").value;

        let cors = "https://cors-anywhere.herokuapp.com";
        let url =
            "https://api.telegram.org/bot1015601247:AAHOgvbkBWspaTwQ_YMA6aosSYzsVrFRHCo/sendMessage";

        if (name.length > 0 && email.length > 0) {
            msg = msg.length > 0 ? msg : "Whatever";

            data = new FormData();

            data.append("chat_id", "-345344736");
            data.append("text", `Имя: ${name}\r\nE-mail: ${email}\r\nСообщение: ${msg}`);

            let xhr = new XMLHttpRequest();

            xhr.open("POST", `${cors}/${url}`, true);

            xhr.onload = function() {
                if (xhr.status == 200) {
                    alert("Спасибо! В скором времени я с Вами свяжусь.");
                } else {
                    alert("Что-то пошло не так...");
                }
            };

            xhr.send(data);
        } else {
            alert("Заполните данные формы.");
        }
    });
};
